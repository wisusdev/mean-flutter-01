import 'dart:convert';

List<ProductModel> productModelFromJson(String str) => List<ProductModel>.from(json.decode(str).map((x) => ProductModel.fromJson(x)));
String productModelToJson(List<ProductModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));


class ProductModel {

    String id;
    String description;
    num stock;
    num price;

    ProductModel({
        required this.id,
        required this.description,
        required this.stock,
        required this.price
    });

    factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json["_id"],
        description: json["description"],
        stock: json["stock"],
        price: json["price"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "description": description,
        "stock": stock,
        "price": price,
    };
}