import 'package:flutter/material.dart';
import 'package:mean_mobile_01/views/products/product_create.dart';
import 'package:mean_mobile_01/views/products/product_list.dart';

void main() {
	runApp(const MyApp());
}

class MyApp extends StatelessWidget {
	const MyApp({super.key});

	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			debugShowCheckedModeBanner: false,
			title: 'Mobile API',
			theme: ThemeData(
				primarySwatch: Colors.blue
			),
			routes: {
				'/' : (context) => const  ShowProducts(),
				'/create-product': (context) => const ProductCreate(),
			},
		);
	}

}