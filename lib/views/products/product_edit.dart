import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mean_mobile_01/models/product_model.dart';
import 'package:mean_mobile_01/services/api_service.dart';

class ProductEdit extends StatefulWidget {

    final ProductModel product;

    const ProductEdit({super.key, required this.product});

    @override
    State<ProductEdit> createState() => _ProductEditState();
}

class _ProductEditState extends State<ProductEdit> {

    String? description;
    num? price, stock;

  	final _formKey = GlobalKey<FormState>();  

    @override
    Widget build(BuildContext context) {
        return SafeArea(
			child: Scaffold(
				appBar: AppBar(
					title: 	const Text("Edit Product"),
					elevation: 0,
				),
				backgroundColor: Colors.grey[200],
				body: Center(
					child: Container(
						child: editProductForm(),
					),
				),
			),
		);
    }

    editProductForm(){
        return Scaffold(
     		body: SafeArea(
				child: SingleChildScrollView(
					padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 30),
					child: Form(
						key: _formKey,
						child: Column(
							crossAxisAlignment: CrossAxisAlignment.center,
							children: [

                                Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                                    child: TextFormField(
                                        initialValue: widget.product.description,
                                        decoration: const InputDecoration(
                                            hintText: 'Description',
                                        ),

                                        validator: (value) {  
                                            if (value == null || value.isEmpty) {  
                                                return 'Please enter some number';  
                                            }  
                                            description = value;
                                            return null;  
                                        },  
                                    ),
                                ),

                                Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                                    child: TextFormField(
                                        initialValue: '${widget.product.price}',
                                        decoration: const InputDecoration(
                                            hintText: 'Price',
                                        ),
                                        
                                        keyboardType: TextInputType.number,
                                        
                                        inputFormatters: [FilteringTextInputFormatter.digitsOnly],

                                        validator: (value) {  
                                            if (value == null || value.isEmpty) {  
                                                return 'Please enter some text';  
                                            }  
                                            price = int.parse(value);
                                            return null;  
                                        },  
                                    ),
                                ),

                                Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                                    child: TextFormField(
                                        initialValue: '${widget.product.stock}',
                                        decoration: const InputDecoration(
                                            hintText: 'Stock',
                                        ),
                                        
                                        keyboardType: TextInputType.number,
                                        
                                        inputFormatters: [FilteringTextInputFormatter.digitsOnly],

                                        validator: (value) {  
                                            if (value == null || value.isEmpty) {  
                                                return 'Please enter some text';  
                                            }  
                                            stock = int.parse(value);
                                            return null;  
                                        },  
                                    ),
                                ),
                                											
								Padding(
									padding: const EdgeInsets.symmetric(vertical: 16.0),
									child: ElevatedButton(
										onPressed: () {
											if (_formKey.currentState!.validate()) {
                                                
                                                updateProduct();

												ScaffoldMessenger.of(context).showSnackBar(
													const SnackBar(content: Text('Processing Data')),
												);
											}
										},
										child: const Text('Submit'),
									),
								),
							],
						),
					),
				),
      		),
    	);
    }

    void updateProduct() async {

        Map<String, dynamic> data = {
            "description" : description,
            "price" : price,
            "stock": stock 
        };

        String id = widget.product.id;

        var response = await ApiService.updateProduct(data, id);

        if(response == true){
            Navigator.pushNamed(context, '/');
        } else {
            const SnackBar(content: Text('Ocurrio un error'));
        }
    }
}