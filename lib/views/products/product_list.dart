import 'package:flutter/material.dart';
import 'package:mean_mobile_01/models/product_model.dart';
import 'package:mean_mobile_01/services/api_service.dart';
import 'package:mean_mobile_01/views/products/product_edit.dart';

class ShowProducts extends StatefulWidget {
	const ShowProducts({super.key});

  	@override
  	State<ShowProducts> createState() => _ShowProductsState();
}

class _ShowProductsState extends State<ShowProducts> {

	bool isApiCallProcess = false;

	@override
  	void initState() {
    	super.initState();
  	}

  	@override
  	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: const Text('Mobile API'),
				elevation: 3,
			),
			backgroundColor: Colors.grey[200],
			body: loadProducts(),
            floatingActionButton: FloatingActionButton(
                onPressed: (){
                    Navigator.pushNamed(context, '/create-product');
                },
                backgroundColor: Colors.amber,
                child: const Icon(Icons.add),
            ),
		);
  	}

	loadProducts() {
		return FutureBuilder(
			future: ApiService.getProducts(),
			builder: (BuildContext context, AsyncSnapshot<List<ProductModel>?> model){
				
                if(model.hasData){
					return productList(model.data);
				}

				return const Center(
					child: Text('¡Ha ocurrido un error!'),
				);
			},
		);
	}

    productList(products){
        return ListView.separated(
            itemCount: products.length,
            itemBuilder: (context, int index) {
                return ListTile(
                    leading: CircleAvatar(
                        backgroundColor: Colors.amber,
                        child: Text('$index', style: const TextStyle(color: Colors.white),),
                    ),
                    title: Text('${products[index].description}'),
                    subtitle: Text('Price: ${products[index].price} and stock ${products[index].stock}'),
                    trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                            IconButton(
                                icon: const Icon(Icons.edit),
                                onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => ProductEdit(product: products[index]),  
                                    ));
                                }
                            ),
                            IconButton(
                                icon: const Icon(Icons.delete),
                                onPressed: () {
                                    showDialog(
                                        context: context, 
                                        builder: (BuildContext context) {
                                            return AlertDialog(
                                                title: const Text('Plase confirm'),
                                                content: const Text('Are you sure to remove the article?'),
                                                actions: [
                                                    TextButton(
                                                        onPressed: () {
                                                            ApiService.deleteProduct(products[index].id).then((response) {
                                                                setState(() {
                                                                    isApiCallProcess = false;
                                                                });
                                                            });
                                                            // Close the dialog
                                                            Navigator.of(context).pop();
                                                        },
                                                        child: const Text('Yes'),
                                                    ),
                                                    TextButton(
                                                        onPressed: () {
                                                            // Close the dialog
                                                            Navigator.of(context).pop();
                                                        },
                                                        child: const Text('No')
                                                    )
                                                ],
                                            );
                                        }
                                    );
                                }, 
                            ),
                        ],
                    ),
                );
            },
            separatorBuilder: (context, index) {
                return const Divider();
            },
        );
    }

}