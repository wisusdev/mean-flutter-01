import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:mean_mobile_01/config/app.dart';
import 'package:mean_mobile_01/models/product_model.dart';

class ApiService {
    static var client = http.Client();

    static Future<List<ProductModel>?> getProducts() async {

        Map<String, String> requestHeaders = {
            'Content-Type': 'application/json',
        };

        var url = Uri.http(Config.apiURI, Config.productApi);
        var response = await http.get(url, headers: requestHeaders);

        if(response.statusCode == 200){
            return productModelFromJson(response.body);
        } else {
            return null;
        }
    }

    static Future<bool> createProduct(Map<String, dynamic> data) async {
        Map<String, String> requestHeaders = {
            'Content-type': 'application/json',
        };

        var url = Uri.http(Config.apiURI, Config.productApi);
        var response = await http.post(url, body: jsonEncode(data), headers: requestHeaders);

        if (response.statusCode == 201) {
            return true;
        } else {
            return false;
        }

    }

    static Future<bool> updateProduct(Map<String, dynamic> data, String productId) async {

        Map<String, String> requestHeaders = {
            'Content-Type': 'application/json',
        };

        var url = Uri.http(Config.apiURI, "${Config.productApi}/$productId");
        var response = await client.put(url, body: jsonEncode(data), headers: requestHeaders);

        if (response.statusCode == 201) {
            return true;
        } else {
            return false;
        }
    }

    static Future<bool> deleteProduct(productId) async {

        Map<String, String> requestHeaders = {
            'Content-Type': 'application/json',
        };

        var url = Uri.http(Config.apiURI, "${Config.productApi}/$productId");

        var response = await client.delete(url, headers: requestHeaders);

        if (response.statusCode == 200) {
            return true;
        } else {
            return false;
        }
    }


}